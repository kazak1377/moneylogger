/**
* @providesModule app/models/wallets
*/

import {Model} from "./base/model";
import schemas from "./base/schemas";

export default class Wallets extends Model {

    key = "";
    value = "";

    static get schema() {
        return schemas.settings;
    }

    constructor(data) {
        super();
        this.initObject(data)
    }
}
