const Realm = require('realm');
import Migrations from '../migrations/all';
import schemas from "./schemas";
import {AsyncStorage} from 'react-native';
import FileSystem from 'react-native-filesystem';

let instance = null;
export default class Db {
    connetion = null;

    constructor() {
        if(!instance){
            this.connetion = this.getRealm();
            instance = this;
        }
        return instance;
    }

    __migrate(exception) {
        console.log ("WE ARE MIGRATING", exception);
        let currentVersion = Realm.schemaVersion(Realm.defaultPath);
        let newVersion = Migrations[Migrations.length - 1].version + currentVersion;
        console.log ("migration versions",currentVersion, newVersion);
        let schema = this.__getSchema();

        try {
            let db = new Realm({
                schema: schema,
                schemaVersion: newVersion,
                migration: function (oldRealm, newRealm) {
                    for (let i = 0; i < Migrations.length; i++) {
                        if (Migrations[i].version > currentVersion) {
                            Migrations[i].migrate(oldRealm, newRealm);
                        }
                    }
                }
            });
            return db;
        } catch (ex) {
            console.log("MIGRATION ERROR: ", ex, Realm.defaultPath);
            FileSystem.delete(Realm.defaultPath).then(() => {console.log('deleted realm')})
        }
    }

    __getSchema() {
        let allSchemas = schemas.getAll();
        //update schema to have an id. So that we
        //don't have to care about it existence.
        allSchemas.map((item) => {
            item.primaryKey = 'id';
            item['properties'].id = 'int';
        });

        return allSchemas;
    }

    /**
     * @return {RealmObject}
     */
    getRealm() {
        let schema = this.__getSchema();
        let currentVersion = Realm.schemaVersion(Realm.defaultPath);

        let realmSavedPath = AsyncStorage.getItem('realmPath');
        if (realmSavedPath) {

        }

        if (currentVersion == -1) {
            currentVersion = 1;
        }

        console.log("getting realm! shcema:", schema, "current version: ", currentVersion);

        let db;
        try {
            db = new Realm({
                schema: schema,
                schemaVersion: currentVersion,
            });
            return db;
        } catch (ex) {
            this.__migrate(ex);
        }
    }

    static getConnection() {
        let db = new Db();
        return db.connection;
    }
}