
let schemas = {
	income : {
		name: 'income',
        properties: {
            amount: 'double',
            date: 'date',
            wallet_id: 'double',
            category_id: 'double',
            profile_id: 'double',
            comment: 'string',
            title: 'string'
        }
	},
	expense: {
        name: 'expense',
        properties: {
            amount: 'double',
            date: 'date',
            wallet_id: 'double',
            category_id: 'double',
            profile_id: 'double',
            comment: 'string',
            title: 'string'
        }
    },
    profile: {
        name: 'profile',
        properties: {
            title : "string",
            color : "string",
            description : "string",
            icon : "string"
        }
    },
    settings: {
        name: 'applicationSettings',
        properties: {
            key : "string",
            value: "string"
        }
    },
	wallets : {
		name: 'wallet',
		properties : {
			title: "string",
			icon: "string"
		}
	},
	categories : {
		name: "category",
		properties: {
			type : "string",
			parent_id: "int",
			title: "string"
		}
	}
};

schemas.getAll = () => {
	let result = [];
	for (let prop in schemas) {
		if (schemas.hasOwnProperty(prop) && typeof schemas[prop] != 'function') {
       		result.push(schemas[prop]);
    	}
	}
	return result;
};
export default schemas;
