import Db from './db';

export class Model {
	id = -1;

	constructor() {
        if (this.constructor === Model) {
            throw new TypeError('Abstract class "Model" cannot '+
				'be instantiated directly.');
        }
    }

    static getCurrentSchemaName() {
        let schema = this.schema;
        return schema.name;
    }

	save () {
		let db = new Db();
		db = db.connetion;

		if (this.id === -1) {
			let allData = db.objects(
				this.constructor.getCurrentSchemaName()
			).sorted('id', true);
			let maxId = 0;

			if (allData.length > 0) {
				maxId = allData.slice(0,1)[0].id;
				maxId++;
			}
			this.id = maxId;
		}
		db.write(() => {
			db.create(this.constructor.getCurrentSchemaName(), this, true);
		});
	}



	initObject(realmObject = null) {
		function* entries(obj) {
		   for (let key of Object.keys(obj)) {
		     yield [key, obj[key]];
		   }
	   	}
		if (realmObject !== null) {
			for (let [key, value] of entries(realmObject)) {
	   			this[key] = value;
			}
		}
	}

	remove() {
        try {
            /** @type {Realm} */
			let db = new Db();
			db = db.connetion;

            db.write(() => {
                db.delete(db.objects(this.constructor.getCurrentSchemaName()).
                    filtered("id="+this.id));
            });
        } catch( ex ) {
           console.error("cant delete", this, ex);
        }
    }

	static find (query = "") {
        let db = new Db();
        db = db.connetion;
		let resultData = [];
		let realmObjects;
		if (query != "") {
			realmObjects =
				db.objects(this.getCurrentSchemaName()).filtered(query);
		} else {
			realmObjects = db.objects(this.getCurrentSchemaName());
		}
		for (let i = 0; i < realmObjects.length; i++) {
			let currentObject = new this(realmObjects[i]);
			currentObject.id = realmObjects[i].id;
			resultData.push(currentObject);
		}
		return resultData;
	}

	static findOne (query = "") {
        let db = new Db();
        db = db.connetion;
		let result;
		if (query != "") {
			result = db.objects(this.getCurrentSchemaName()).filtered(query);
		} else {
			result = db.objects(this.getCurrentSchemaName());
		}
        let resultData = [];
        for (let i = 0; i < result.length; i++) {
            let currentObject = new this(result[i]);
            currentObject.id = result[i].id;
            resultData.push(currentObject);
        }

		if (resultData.length > 0) {
			return resultData[0];
		} else {
			return false;
		}
	}

	static findById(id) {
        return this.findOne("id='"+id+"'");
    }
}
