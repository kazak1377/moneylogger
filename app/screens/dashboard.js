/*
 * @providesModule app/screens/dashboard
 */
import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ListView,
    View,
    Text
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    Card,
} from "native-base";
import {Column as Col, Row} from 'react-native-flexbox-grid';
import { Actions } from 'react-native-router-flux';
import Expense from "app/models/expense";
import Income from "app/models/income";
import Profiles from "app/models/profiles"
import Strings from "app/resources/strings";
import globals from "app/resources/globals";
import TotalBalanceHelper from 'app/helpers/totalBalance';

export default class DashboardScreen extends Component {
    constructor(props) {
        super(props);
        let total = TotalBalanceHelper.getBalance();
        this.state = {
            totalBalance : total,
            totalBalanceString : total.toFixed(2)
                + globals.settings.curr,
            profiles: Profiles.find()
        }
    }

    refresh() {
        let total = TotalBalanceHelper.getBalance();
        this.setState({
            totalBalance : total,
            totalBalanceString : total.toFixed(2)
                + globals.settings.curr
        });
    }

    getCard(icon, color, title, description, totalBalanceString, id) {
        return (
            <Card key={id} style={{padding: 10}}>
                <Row>
                    <Col sm={9}>
                        <Row>
                            <Icon name={icon} style={{padding: 5, color: color}}/>
                            <Text style={{fontSize:18,paddingTop: 10, paddingLeft: 5}}>
                                {title}
                            </Text>
                        </Row>
                    </Col>
                    <Col sm={3}>
                        <Row style={{justifyContent: "flex-end"}}>
                            <Button
                                rounded
                                success
                                style={{marginRight: 5}}
                                onPress={() => {
                                    Actions.creator({
                                    profileId: id,
                                    from: 'dashboard',
                                    transactionType: 'income'})
                                }}
                                title="">
                                <Icon name="ios-add-circle-outline" />
                            </Button>
                            <Button rounded title="" danger onPress={() => {
                               Actions.creator({
                                    profileId: id,
                                    from: 'dashboard',
                                    transactionType: 'expense'})
                                }}>
                                <Icon name="ios-remove-circle-outline" />
                            </Button>
                        </Row>
                    </Col>
                </Row>
                <Row style={{backgroundColor: 'transparent'}}>
                    <Text style={{color: "#999999", fontSize: 13}}>
                        {description}
                    </Text>
                </Row>
                <Row>
                    <Col style={{alignItems: 'flex-end', justifyContent: "flex-end"}}>
                        <Text style={{fontSize: 32}}>
                            {totalBalanceString}
                        </Text>
                    </Col>
                </Row>
            </Card>
        );
    }

    render() {
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions.refresh({key: 'drawer', open: value => !value })
                    }} title="">
                        <Icon name='ios-menu' />
                    </Button>
                    <Title>{Strings.i18n('dashboard')}</Title>
                </Header>
                <Content style={{padding: 10}}>
                    <Card key={-1} style={{padding: 10}}>
                        <Row>
                            <Icon name="ios-bulb" style={{padding: 5, color: "#FFCC00"}}/>
                            <Text style={{fontSize: 19,paddingTop: 10, paddingLeft: 5}}>
                                {Strings.i18n('totalBalance')}
                            </Text>
                        </Row>
                        <Row>
                            <Col style={{alignItems: 'flex-end', justifyContent: "flex-end"}}>
                                <Text style={{fontSize: 32}}>
                                    {this.state.totalBalanceString}
                                </Text>
                            </Col>
                        </Row>
                    </Card>
                    {
                        this.state.profiles.map((profile) => {
                            let totalBalance = 0.0;
                            let profileIncomes = Income.find('profile_id="'+profile.id+'"');
                            profileIncomes.map((income) => {
                                totalBalance += income.amount;
                            });

                            let profileExpenses = Expense.find('profile_id="'+profile.id+'"');
                            profileExpenses.map((expense) => {
                                totalBalance -= expense.amount;
                            });

                            let totalBalanceString = totalBalance.toFixed(2) + globals.settings.curr;
                            return this.getCard(
                                profile.icon,
                                profile.color,
                                profile.title,
                                profile.description,
                                totalBalanceString,
                                profile.id
                            );
                        })
                    }
                </Content>
            </Container>

        );
    }
}
