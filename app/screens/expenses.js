/*
* @providesModule app/screens/expenses
*/
import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    View
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
} from "native-base";
import { Actions } from 'react-native-router-flux';
import Expense from "app/models/expense"
import ARList from "app/components/ARList"
import Strings from "app/resources/strings";

export default class ExpensesScreen extends Component {
	constructor(props) {
        super(props);
        if (props.currentProfile) {
            this.state = {
                currentProfile: props.currentProfile
            }
        } else {
            this.state = {
                currentProfile: false
            }
        }
	}

    render() {
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions.refresh({key: 'drawer', open: value => !value })
                    }}>
                        <Icon name='ios-menu' />
                    </Button>
                    <Title >
                        {Strings.i18n('expensesList')}
                    </Title>
                    <Button transparent onPress={
                        () => {
                            Actions.creator({transactionType: 'expense', from: 'expenses'})
                        }
                    }>
                        <Icon name='ios-add' />
                    </Button>
                </Header>
                <Content style={{padding: 5}}>
                    <ARList
                        dataArray={Expense.find()}
                        toCreator="expenses"
                        icon={Expense.icon}
                        refresh={() => {
                            return Expense.find();
                        }}
                    />
                </Content>
            </Container>

        );
    }
}
