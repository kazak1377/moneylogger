/*
* @providesModule app/screens/incomes
*/
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon
} from "native-base";
import Income from "app/models/income";
import ARList from "app/components/ARList";
import Strings from "app/resources/strings";
import styles from 'app/resources/appStyles';

export default class IncomesScreen extends Component {
  render() {
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions.refresh({key: 'drawer', open: value => !value })
                    }}>
                        <Icon name='ios-menu' />
                    </Button>
                    <Title>{Strings.i18n('incomes')}</Title>
                    <Button transparent onPress={
                        () => {
                            Actions.creator({transactionType: 'income', from: 'incomes'})
                        }
                    }>
                        <Icon name='ios-add' />
                    </Button>
                </Header>
                <Content style={{padding: 5}}>
                    <ARList
                        dataArray={Income.find()}
                        toCreator="incomes"
                        icon={Income.icon}
                        refresh={() => {
                            return Income.find();
                        }}
                    />
                </Content>
            </Container>

        );
    }
}