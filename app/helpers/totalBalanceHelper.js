/**
 * @providesModule app/helpers/totalBalance
*/
import Expense from "app/models/expense";
import Income from "app/models/income";

export default class TotalBalanceHelper {
    static getBalance() {
        let allIncomes = Income.find();
        let allExpense = Expense.find();
        let total = 0.0;
        allIncomes.map((income) => {
            total += income.amount;
        });
        allExpense.map((expense) => {
            total -= expense.amount;
        });
        return total;
    }
}