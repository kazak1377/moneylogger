/*
* @providesModule app/components/ARList
 */
import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    View
} from "react-native";
import {
    Icon,
    List,
    ListItem,
} from "native-base";
import { Actions } from 'react-native-router-flux';
import globals from "app/resources/globals";
import Swipeout from 'libs/Swipeout';
console.log("OK!!__");

export default class ARList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            elements: this.props.dataArray
        }
    }

    refresh() {
        this.setState({
            elements: this.props.refresh()
        });
    }

    /**
     * Renders our list element
     * @param {Expense | Income} e
     * @returns {XML}
     */
    renderRow(e) {
        let swipeoutButtons = [
            {
                text: (<Icon name="ios-trash" style={{color: 'white'}}/>),
                backgroundColor: "red",
                onPress: () => {
                    e.remove();
                    this.refresh();
                }
            }
        ];
        let elementProfile = e.getProfile();
        return (
            <Swipeout right={swipeoutButtons}
                      backgroundColor="white"
                      autoClose={true}>
                <ListItem
                    iconLeft
                    key={e.date}
                    onPress={() => {
                        Actions.creator({
                            element: e,
                            from: this.props.toCreator
                        })}
                    }
                >
                    <Icon name={elementProfile.icon} style={{color: elementProfile.color}}/>
                    <Text style={{marginLeft: 10}}>{e.comment}</Text>
                    <Text>{e.amount} {globals.settings.curr}</Text>
                </ListItem>
            </Swipeout>
        );
    }

    render() {
        return (
            <List
                dataArray={this.state.elements}
                renderRow={(row) => this.renderRow(row)}>
            </List>
        );
    }
}
