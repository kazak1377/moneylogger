/**
* @providesModule models/profiles
*/

import {Model} from "./base/model";
import schemas from "./base/schemas";

export default class Profiles extends Model {

    title = "";
    color = "";
    description = "";
    icon = "";

    static get schema() {
        return schemas.profile;
    }

    constructor(data) {
        super();
        this.initObject(data)
    }
}
