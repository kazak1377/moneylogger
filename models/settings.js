/**
* @providesModule models/settings
*/

import {Model} from "./base/model";
import schemas from "./base/schemas";
import globals from "resources/globals";

export default class Settings extends Model {

    key = "";
    value = "";

    static get schema() {
        return schemas.settings;
    }

    constructor(data) {
        super();
        this.initObject(data)
    }

    static getValue(key) {
        let settingsItem = this.find("key='"+key+"'");
        if (settingsItem.length) {
            return settingsItem[0].value;
        } else {
            return false;
        }
    }

    static setValue(key, value) {
        let settingsItem;
        settingsItem = this.find("key='"+key+"'");
        if (!settingsItem.length) {
            settingsItem = new Settings();
            settingsItem.key = key;
        } else {
            settingsItem = settingsItem[0];
        }
        settingsItem.value = value;
        settingsItem.save();
        if (globals.settings.hasOwnProperty(key)) {
            globals.settings[key] = value;
        }
    }
}
