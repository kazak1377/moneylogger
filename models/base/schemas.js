
export default schemas = {
	income : {
		name: 'income',
        properties: {
            amount: 'double',
            date: 'date',
            wallet_id: 'double',
            category_id: 'double',
            profile_id: 'double',
            comment: 'string',
            title: 'string'
        }
	},
	expense: {
        name: 'expense',
        properties: {
            amount: 'double',
            date: 'date',
            wallet_id: 'double',
            category_id: 'double',
            profile_id: 'double',
            comment: 'string',
            title: 'string'
        }
    },
    profile: {
        name: 'profile',
        properties: {
            title : "string",
            color : "string",
            description : "string",
            icon : "string"
        }
    },
    settings: {
        name: 'applicationSettings',
        properties: {
            key : "string",
            value: "string"
        }
    }
};

schemas.getAll = () => {
	let result = [];
	for (let prop in schemas) {
		if (schemas.hasOwnProperty(prop) && typeof schemas[prop] != 'function') {
       		result.push(schemas[prop]);
    	}
	}
	return result;
}