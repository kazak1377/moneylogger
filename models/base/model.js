const Realm = require('realm');
import Migrations from '../migrations/all';
import schemas from "./schemas";

export class Model {
	id = -1;

	constructor() {
        if (this.constructor === Model) {
            throw new TypeError('Abstract class "Model" cannot '+
				'be instantiated directly.');
        }
    }

	save () {
		let schema = this.constructor.getSchema();
		/** @type {Realm} */
		let db;
		try {
			console.log ('saving', this);
			console.log ('saving schema', schema);

            db = this.constructor.getRealm();

            console.log("realm: ", db, Realm.defaultPath);
		} catch( ex ) {
			db = this.constructor.migrate(ex);
		}

		if (this.id === -1) {
			let allData = db.objects(this.constructor.getCurrentSchemaName()).sorted('id', true);
			let maxId = 0;

			if (allData.length > 0) {
				maxId = allData.slice(0,1)[0].id;
				maxId++;
			}
			this.id = maxId;
		}
		db.write(() => {
			db.create(this.constructor.getCurrentSchemaName(), this, true);
		});
	}

	static migrate(exception) {
		console.log ("WE ARE MIGRATING", exception);
		let currentVersion = Realm.schemaVersion(Realm.defaultPath);
		let newVersion = Migrations[Migrations.length - 1].version * currentVersion;

		console.log ("migration versions",currentVersion, newVersion);

		let schema = this.getSchema();
		let db = new Realm({
			schema : schema,
			schemaVersion : newVersion,
			migration : function(oldRealm, newRealm) {
				for (let i = 0; i < Migrations.length; i++) {
					if (Migrations[i].version > currentVersion) {
						Migrations[i].migrate(oldRealm, newRealm);
					}
				}
			}
		});
		return db;
	}

	initObject(realmObject = null) {
		function* entries(obj) {
		   for (let key of Object.keys(obj)) {
		     yield [key, obj[key]];
		   }
	   	}
		if (realmObject !== null) {
			for (let [key, value] of entries(realmObject)) {
	   			this[key] = value;
			}
		}
	}

	remove() {
        try {
            /** @type {Realm} */
            let db = this.constructor.getRealm();

            db.write(() => {
                db.delete(db.objects(this.constructor.getCurrentSchemaName()).
                    filtered("id="+this.id));
            });
        } catch( ex ) {
           console.error("cant delete", this);
        }
    }

	static getSchema() {
		let allSchemas = schemas.getAll();
		//update schema to have an id. So that we
		//don't have to care about it existence.
		allSchemas.map((item) => {
			item.primaryKey = 'id';
			item['properties'].id = 'int';
		});
	
		return allSchemas;
	}

	static getCurrentSchemaName() {
		let schema = this.schema;
		return schema.name;
	}

	/**
	 * @return {RealmObject}
	 */
	static getRealm() {
		let schema = this.getSchema();
		let currentVersion = Realm.schemaVersion(Realm.defaultPath);

        let db;

		db = new Realm({
			schema : schema,
			schemaVersion : currentVersion,
		});
        return db;
	}

	static find (query = "") {
        let db;
		let resultData = [];
		let realmObjects;
		try {
            db = this.getRealm();
		} catch (ex) {
			console.log("We got this error, while trying to get data", ex);
			db = this.migrate(ex);
            console.log(db, db.path);
		}
		if (query != "") {
			realmObjects =
				db.objects(this.getCurrentSchemaName()).filtered(query);
		} else {
			realmObjects = db.objects(this.getCurrentSchemaName());
		}
		for (let i = 0; i < realmObjects.length; i++) {
			let currentObject = new this(realmObjects[i]);
			currentObject.id = realmObjects[i].id;
			resultData.push(currentObject);
		}
		return resultData;
	}

	static findOne (query = "") {
		let db;
		try {
			db = this.getRealm();
		} catch (ex) {
			console.log("We got this error, while trying to get data", ex);
			db = this.migrate(ex);
			console.log(db, db.path);
		}
		let result;
		if (query != "") {
			result = db.objects(this.getCurrentSchemaName()).filtered(query);
		} else {
			result = db.objects(this.getCurrentSchemaName());
		}
        let resultData = [];
        for (let i = 0; i < result.length; i++) {
            let currentObject = new this(result[i]);
            currentObject.id = result[i].id;
            resultData.push(currentObject);
        }

		if (resultData.length > 0) {
			return resultData[0];
		} else {
			return false;
		}
	}

	static findById(id) {
        return this.findOne("id='"+id+"'");
    }
}
