/**
 * @providesModule models/income
 */

import {Model} from "./base/model";
import schemas from "./base/schemas";
import Profiles from "./profiles";

export default class Income extends Model {

    amount = 0;
    date = new Date();

    wallet_id = 0;
    category_id = 0;
    profile_id = 0;
    comment = "";
    title = "";

    static icon = "ios-trending-up";

    static get schema() {
        return schemas.income;
    }

    constructor(data) {
        super();
        this.initObject(data)
    }

    getProfile() {
        return Profiles.findById(this.profile_id);
    }
}
