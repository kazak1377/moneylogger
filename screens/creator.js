/*
* @providesModule screens/creator
*/
import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    View
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    List,
    ListItem,
    Picker,
    InputGroup,
    Input
} from "native-base";
import { Actions } from 'react-native-router-flux';
import Strings from "resources/strings";
import globals from "resources/globals";
import styles from "resources/appStyles";
import Expense from "models/expense";
import Profiles from "models/profiles";
import Income from "models/income";
import DatePicker from "react-native-datepicker";
import moment from "moment"

export default class CreatorScreen extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        if (props.element) {
            let elementClass = props.element.constructor.name;
            console.log(elementClass);
            let isIncome = elementClass === "Income"
            this.state = {
                selectedType: isIncome ? "income" : "expense",
                element : props.element,
                date: moment(props.element.date).format("YYYY-MM-DD HH:mm"),
                amount: props.element.amount,
                comment: props.element.comment,
                from: props.from,
                selectedProfileId : props.element.profile_id
            }
        } else {
            this.state = {
                selectedType: props.transactionType,
                date: moment().format("YYYY-MM-DD HH:mm"),
                amount: 0,
                comment: "",
                from: props.from,
                selectedProfileId : props.profileId ? props.profileId :
                    globals.settings.currentProfile.id
            }
        }
        console.log(this.state);
    }

    onValueChange (value: string) {
        this.setState({
            selectedType : value,
        });
    }

    onProfileChange(value: string) {
        this.setState({
            selectedProfileId: value
        });
    }

    onAddPressed() {
        console.log(this.state);
        let newElement;
        if (this.state.selectedType == "expense") {
            newElement = new Expense();
        } else {
            newElement = new Income();
        }
        let element = this.state.element ?
            this.state.element : newElement;
        element.amount = parseFloat(this.state.amount);
        element.comment = this.state.comment;
        element.date = moment(this.state.date,
            "YYYY-MM-DD HH:mm").toDate();
        element.profile_id = this.state.selectedProfileId;
        element.save();
        Actions[this.state.from]();
    }

    amountEnter(amount) {
        console.log("Amount:", parseFloat(amount), amount, amount);
        if (amount == "") {
            amount = 0;
        }
        if (amount[amount.length - 1] != ".") {
            this.setState({amount: parseFloat(amount)});
        } else {
            this.setState({amount: amount});
        }

    }

    render() {
        let allProfiles = Profiles.find();
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions[this.state.from]({type: 'back'})
                    }}>
                        <Icon name='ios-arrow-back' />
                    </Button>
                    <Title>{this.state.element ? Strings.i18n('edit') : Strings.i18n('add')}</Title>
                </Header>
                <Content>
                    <List style={{marginRight:15}}>
                        <ListItem>
                            <Text>{Strings.i18n('type')}</Text>
                            <Picker
                                iosHeader={Strings.i18n('selectType')}
                                mode="dropdown"
                                selectedValue={this.state.selectedType}
                                onValueChange={this.onValueChange.bind(this)}>
                                <Picker.Item label={Strings.i18n('expense')}
                                             value="expense" />
                                <Picker.Item label={Strings.i18n('income')}
                                             value="income" />
                            </Picker>
                        </ListItem>
                        <ListItem>
                            <Text>{Strings.i18n('profile')}</Text>
                            <Picker
                                iosHeader={Strings.i18n('selectType')}
                                mode="dropdown"
                                selectedValue={this.state.selectedProfileId}
                                onValueChange={this.onProfileChange.bind(this)}>
                                {
                                    allProfiles.map((profile) => {
                                        return (
                                            <Picker.Item
                                                key={profile.id}
                                                label={profile.title}
                                                value={profile.id} />
                                        );
                                    })
                                }
                            </Picker>
                        </ListItem>
                        <ListItem>
                            <DatePicker
                                customStyles={{dateInput: {
                                    //here we style out input
                                    borderColor: '#fff'
                                }}}
                                date={this.state.date}
                                mode="datetime"
                                format="YYYY-MM-DD HH:mm"
                                confirmBtnText={Strings.i18n('confirm')}
                                cancelBtnText={Strings.i18n('cancel')}
                                showIcon={false}
                                onDateChange={(date) => {
                                    this.setState({date: date});
                                }}
                            />
                        </ListItem>
                        <ListItem>
                            <InputGroup>
                                <Input inlineLabel
                                       value={this.state.amount+""}
                                       keyboardType="numeric"
                                       onChangeText={(amount) => {
                                           this.amountEnter(amount);
                                       }}
                                       label={Strings.i18n('amount')}/>
                            </InputGroup>
                        </ListItem>

                        <ListItem>
                            <InputGroup >
                                <Input inlineLabel
                                       value={this.state.comment}
                                       onChangeText={(string) => {
                                           this.setState({comment: string})
                                       }}
                                       label={Strings.i18n('comment')}/>
                            </InputGroup>
                        </ListItem>
                    </List>
                    <Content>
                        <Button block
                                success
                                onPress={this.onAddPressed.bind(this)}
                                style={{marginLeft: 10, marginRight: 10, marginTop: 5}}>
                            {Strings.i18n('save')}
                        </Button>
                    </Content>
                </Content>
            </Container>

        );
    }
}