/*
 * @providesModule screens/profileEdit
 */
import React, { Component } from 'react';
import { View, Text, AlertIOS } from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
    Container,
    Header,
    Footer,
    Title,
    Content,
    Button,
    ListItem,
    Picker,
    InputGroup,
    Row,
    Input,
    Icon,
    List
} from "native-base";
import Strings from "resources/strings";
import styles from 'resources/appStyles';
import Profiles from 'models/profiles';

export default class ProfileEditScreen extends Component {
    static possibleIcons = [
        "ios-podium",
        "ios-woman",
        "ios-walk",
        "ios-water",
        "ios-warning",
        "ios-videocam",
        "logo-usd",
        "ios-umbrella",
        "logo-tux",
        "ios-trophy",
        "ios-timer",
        "ios-stats",
        "ios-snow",
        "ios-shirt",
        "ios-school",
        "ios-rose",
        "ios-ribbon",
        "ios-print",
        "ios-planet",
        "ios-plane",
        "ios-pizza"
    ]

    constructor(props) {
        super(props);
        console.log(props, !!props.element);
        if (props.element) {
            console.log('hey!', props, this.props);
            this.state = {
                element: props.element
            }
        } else {
            let newProfile = new Profiles();
            newProfile.icon = "ios-podium"
            newProfile.title = Strings.i18n('newProfile');
            newProfile.description = "";
            newProfile.color = "#4CD964";
            this.state = {
                element: newProfile
            }
        }
        console.log(this.state.element.icon);
    }

    updateElement(prop, newVal) {
        let buff = this.state.element;
        buff[prop] = newVal;
        this.setState({
            element : buff
        });
        console.log(this.state.element)
    }

    onIconChange (value: string) {
        this.updateElement('icon', value);
    }

    onColorChange(value: string) {
        this.updateElement('color', value);
    }

    render() {
        let colors = [
            "#FF3B30", //red
            "#34AADC", //light blue
            "#4CD964", //green
            "#FF9500", //orange
            "#C644FC"  //purple
        ];
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        this.state.element.save();
                        Actions.pop({refresh: {doThis: true}});
                    }}>
                        <Icon name='ios-arrow-back' />
                    </Button>
                    <Title>{Strings.i18n('profileSettings')}</Title>
                </Header>
                <Content>
                    <List style={{marginRight:15}}>
                        <ListItem key="row0">
                            <Text>{Strings.i18n('color')}</Text>
                            <Picker
                                iosHeader={Strings.i18n('color')}
                                mode="dropdown"
                                selectedValue={
                                    colors.indexOf(this.state.element.color) ?
                                        this.state.element.color :
                                        colors[0]
                                }
                                onValueChange={this.onColorChange.bind(this)}
                            >
                                <Picker.Item value="#FF3B30" label={Strings.i18n("red")} />
                                <Picker.Item value="#34AADC" label={Strings.i18n("lightBlue")} />
                                <Picker.Item value="#4CD964" label={Strings.i18n("green")} />
                                <Picker.Item value="#FF9500" label={Strings.i18n("orange")} />
                                <Picker.Item value="#C644FC" label={Strings.i18n("purple")} />
                            </Picker>
                        </ListItem>
                        <ListItem key="row1">
                            <Icon name={this.state.element.icon}
                                  style={{color: this.state.element.color}}/>
                            <Picker
                                iosHeader={Strings.i18n('selectIcon')}
                                mode="dropdown"
                                selectedValue={this.state.element.icon}
                                onValueChange={this.onIconChange.bind(this)}>
                                <Picker.Item value="ios-podium" label={Strings.i18n("podium")} />
                                <Picker.Item value="ios-woman" label={Strings.i18n("woman")} />
                                <Picker.Item value="ios-walk" label={Strings.i18n("walk")} />
                                <Picker.Item value="ios-water" label={Strings.i18n("water")} />
                                <Picker.Item value="ios-warning" label={Strings.i18n("warning")} />
                                <Picker.Item value="ios-videocam" label={Strings.i18n("videocam")} />
                                <Picker.Item value="ios-umbrella" label={Strings.i18n("umbrella")} />
                                <Picker.Item value="ios-trophy" label={Strings.i18n("trophy")} />
                                <Picker.Item value="ios-timer" label={Strings.i18n("timer")} />
                                <Picker.Item value="ios-stats" label={Strings.i18n("stats")} />
                                <Picker.Item value="ios-snow" label={Strings.i18n("snow")} />
                                <Picker.Item value="ios-shirt" label={Strings.i18n("shirt")} />
                                <Picker.Item value="ios-school" label={Strings.i18n("school")} />
                                <Picker.Item value="ios-rose" label={Strings.i18n("rose")} />
                                <Picker.Item value="ios-ribbon" label={Strings.i18n("ribbon")} />
                                <Picker.Item value="ios-print" label={Strings.i18n("print")} />
                                <Picker.Item value="ios-planet" label={Strings.i18n("planet")} />
                                <Picker.Item value="ios-plane" label={Strings.i18n("plane")} />
                                <Picker.Item value="ios-pizza" label={Strings.i18n("pizza")} />
                                <Picker.Item value="logo-usd" label={Strings.i18n("usd")} />
                                <Picker.Item value="logo-tux" label={Strings.i18n("tux")} />
                            </Picker>
                        </ListItem>
                        <ListItem key="row2">
                            <InputGroup>
                                <Input inlineLabel
                                       value={this.state.element.title}
                                       onChangeText={(title) => {
                                           this.updateElement('title',title);
                                       }}
                                       //onEndEditing={() => {this.state.element.save()}}
                                       label={Strings.i18n('title')}/>
                            </InputGroup>
                        </ListItem>
                        <ListItem key="row3">
                            <InputGroup>
                                <Input inlineLabel
                                       value={this.state.element.description}
                                       onChangeText={(description) => {
                                           this.updateElement('description',description);
                                       }}
                                       label={Strings.i18n('description')}/>
                            </InputGroup>
                        </ListItem>
                    </List>
                </Content>
            </Container>

        );
    }
}