/*
* @providesModule screens/incomes
*/
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    Card,
    ListItem,
    InputGroup,
    Input
} from "native-base";
import Income from "models/income";
import ARList from "components/ARList";
import Strings from "resources/strings";
import styles from 'resources/appStyles';

export default class IncomesScreen extends Component {
  render() {
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions.refresh({key: 'drawer', open: value => !value })
                    }}>
                        <Icon name='ios-menu' />
                    </Button>
                    <Title>{Strings.i18n('incomes')}</Title>
                    <Button transparent onPress={
                        () => {
                            Actions.creator({transactionType: 'income', from: 'incomes'})
                        }
                    }>
                        <Icon name='ios-add' />
                    </Button>
                </Header>
                <Content style={{padding: 5}}>
                    <ARList
                        dataArray={Income.find()}
                        toCreator="incomes"
                        icon={Income.icon}
                        refresh={() => {
                            return Income.find();
                        }}
                    />
                </Content>
            </Container>

        );
    }
}