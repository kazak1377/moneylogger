/*
 * @providesModule screens/dashboard
 */
import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ListView,
    View
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Row,
    Col,
    Button,
    Icon,
    Text,
    Card,
    CardItem,
    ListItem,
    InputGroup,
    Input
} from "native-base";
import { Actions } from 'react-native-router-flux';
import Expense from "models/expense";
import Income from "models/income";
import Profiles from "models/profiles"
import Strings from "resources/strings";
import styles from "resources/appStyles";
import ARCardTitle from "components/ARCardTitle";
import globals from "resources/globals";

export default class DashboardScreen extends Component {
    constructor(props) {
        super(props);
        let allIncomes = Income.find();
        let allExpense = Expense.find();
        let total = 0.0;
        allIncomes.map((income) => {
            total += income.amount;
        });
        allExpense.map((expense) => {
            total -= expense.amount;
        });
        this.state = {
            totalBalance : total,
            totalBalanceString : total.toFixed(2)
                + globals.settings.curr,
            profiles: Profiles.find()
        }
    }

    refresh() {
        let allIncomes = Income.find();
        let allExpense = Expense.find();
        let total = 0.0;
        allIncomes.map((income) => {
            total += income.amount;
        });
        allExpense.map((expense) => {
            total -= expense.amount;
        });
        this.setState({
            totalBalance : total,
            totalBalanceString : total.toFixed(2)
                + globals.settings.curr
        });
    }

    getCard(icon, color, title, description, totalBalanceString, id) {
        return (
            <Card key={id} style={{marginBottom: 10, height: 140}}>
                <Row style={{height:40}}>
                    <Col>
                        <Row style={{paddingTop: 10, paddingLeft: 10, paddingRight: 10, height: 25}}>
                            <Icon name={icon} style={{padding: 5, color: color}}/>
                            <Text style={{fontSize:18,paddingTop: 10, paddingLeft: 5}}>
                                {title}
                            </Text>
                        </Row>
                        <Row style={{paddingLeft: 15, backgroundColor: 'transparent'}}>
                            <Text style={{color: "#999999", fontSize: 13}}>
                                {description}
                            </Text>
                        </Row>
                    </Col>
                    <Col>
                        <Row style={{justifyContent: "flex-end", paddingTop: 10, paddingLeft: 10, paddingRight: 10,}}>
                            <Button
                                rounded
                                success
                                style={{marginRight: 5}}
                                onPress={() => {
                                    Actions.creator({
                                        profileId: id,
                                        from: 'dashboard',
                                        transactionType: 'income'})
                                }}
                            >
                                <Icon name="ios-add-circle-outline" />
                            </Button>
                            <Button rounded danger onPress={() => {
                                Actions.creator({
                                    profileId: id,
                                    from: 'dashboard',
                                    transactionType: 'expense'})
                            }}>
                                <Icon name="ios-remove-circle-outline" />
                            </Button>
                        </Row>
                    </Col>
                </Row>
                <Row style={{padding: 10, height: 50}}>
                    <Row style={{justifyContent: "flex-end"}}>
                        <Text style={{fontSize: 32, paddingTop:16}}>
                            {totalBalanceString}
                        </Text>
                    </Row>
                </Row>
            </Card>
        );
    }

    render() {
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions.refresh({key: 'drawer', open: value => !value })
                    }}>
                        <Icon name='ios-menu' />
                    </Button>
                    <Title>{Strings.i18n('dashboard')}</Title>
                </Header>
                <Content style={{padding: 10}}>
                    <Card key={-1} style={{marginBottom: 10, height: 130}}>
                        <Row style={{height:30}}>
                            <Col>
                                <Row style={{paddingTop: 10, paddingLeft: 10, paddingRight: 10, height: 25}}>
                                    <Icon name="ios-bulb" style={{padding: 5, color: "#FFCC00"}}/>
                                    <Text style={{fontSize:18,paddingTop: 10, paddingLeft: 5}}>
                                        {Strings.i18n('totalBalance')}
                                    </Text>
                                </Row>
                                <Row style={{paddingLeft: 15, backgroundColor: 'transparent'}}>
                                    <Text style={{color: "#999999", fontSize: 13}}>
                                    </Text>
                                </Row>
                            </Col>
                        </Row>
                        <Row style={{padding: 10, height: 50}}>
                            <Row style={{justifyContent: "flex-end"}}>
                                <Text style={{fontSize: 32, paddingTop:16}}>
                                    {this.state.totalBalanceString}
                                </Text>
                            </Row>
                        </Row>
                    </Card>

                    {
                        this.state.profiles.map((profile) => {
                            let totalBalance = 0.0;
                            let profileIncomes = Income.find('profile_id="'+profile.id+'"');
                            profileIncomes.map((income) => {
                                totalBalance += income.amount;
                            });

                            let profileExpenses = Expense.find('profile_id="'+profile.id+'"');
                            profileExpenses.map((expense) => {
                                totalBalance -= expense.amount;
                            });

                            let totalBalanceString = totalBalance.toFixed(2) + globals.settings.curr;
                            return this.getCard(
                                profile.icon,
                                profile.color,
                                profile.title,
                                profile.description,
                                totalBalanceString,
                                profile.id
                            );
                        })
                    }
                </Content>
            </Container>

        );
    }
}
