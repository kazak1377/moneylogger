/*
 * @providesModule screens/profileData
 */
import React, { Component } from 'react';
import { View, Text, AlertIOS } from 'react-native';
import { Actions} from 'react-native-router-flux';

import {
    Container,
    Header,
    Footer,
    Title,
    Content,
    Button,
    ListItem,
    Icon,
    List
} from "native-base";
import Strings from "resources/strings";
import styles from 'resources/appStyles';
import Profiles from 'models/profiles';
var Swipeout = require('react-native-swipeout');

export default class ProfileDataScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(nextProps) {
        this.refresh();
    }

    renderRow(e) {
        var swipeoutButtons = [
            {
                text: (<Icon name="ios-trash" style={{color: 'white'}}/>),
                key: e.id,
                backgroundColor: "red",
                onPress: () => {
                    e.remove();
                    this.refresh();
                }
            }
        ]
        return (
            <Swipeout right={swipeoutButtons}
                      key={e.id}
                      backgroundColor="white"
                      autoClose={true}>
                <ListItem iconLeft key={e.id}>
                    <Icon name={e.icon ? e.icon : "ios-cash"} style={{color: e.color}}/>
                    <Text>   {e.title}</Text>
                </ListItem>
            </Swipeout>
        );
    }

    refresh() {
        //todo: implement it
    }

    render() {
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions.profileSettings({type: 'back'})
                    }}>
                        <Icon name='ios-arrow-back' />
                    </Button>
                    <Title>{Strings.i18n('profiles')}</Title>
                    <Button transparent onPress={() => {
                        Actions.profileEdit();
                    }}>
                        <Icon name='ios-add' />
                    </Button>
                </Header>
                <Content>
                    <List
                        dataArray={this.state.elements}
                        refresh={() => {this.setState({
                            elements: Profiles.find()
                        })}}
                        renderRow={(row) => this.renderRow(row)}>
                    </List>
                </Content>
            </Container>
        );
    }
}