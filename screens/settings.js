/*
 * @providesModule screens/settings
 */
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
    Container,
    Header,
    Footer,
    FooterTab,
    Title,
    Content,
    Button,
    Icon,
    Picker,
    Card,
    CardItem,
    ListItem,
    InputGroup,
    Input
} from "native-base";
import ARList from "components/ARList";
import Strings from "resources/strings";
import styles from 'resources/appStyles';
import Settings from 'models/settings';
import globals from "resources/globals";

export default class SettingsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedType : Settings.getValue('lang') ?
                Settings.getValue('lang') :
                globals.settings.lang,
            currency : Settings.getValue('curr') ?
                Settings.getValue('curr') :
                globals.settings.curr
        }
    }

    onValueChange (value: string) {
        this.setState({
            selectedType : value,
        });
        Settings.setValue('lang',value);
    }

    render() {
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions.refresh({key: 'drawer', open: value => !value })
                    }}>
                        <Icon name='ios-menu' />
                    </Button>
                    <Title>{Strings.i18n('settings')}</Title>
                </Header>
                <Content style={{padding: 10}}>
                    <Card>
                        <CardItem>
                            <Text style={styles.blue.text}>{Strings.i18n('language')}</Text>
                            <Picker
                                style={{alignSelf: 'flex-end'}}
                                iosHeader={Strings.i18n('selectType')}
                                mode="dropdown"
                                selectedValue={this.state.selectedType}
                                onValueChange={this.onValueChange.bind(this)}>
                                <Picker.Item label="English"
                                             value="en" />
                                <Picker.Item label="Русский"
                                             value="ru" />
                            </Picker>
                        </CardItem>
                        <CardItem>
                            <Text style={styles.blue.text}>{Strings.i18n('currency')}</Text>
                            <Input inlineLabel
                                   style={styles.blue.input}
                                   value={this.state.currency}
                                   onChangeText={(string) => {
                                       this.setState({currency: string});
                                       Settings.setValue('curr', string);
                                       globals.settings.curr = string;
                                   }}/>
                        </CardItem>
                    </Card>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button onPress={() => {
                            Actions.profileSettings({animation: 'vertical'});
                        }}>
                            {Strings.i18n('profilesSettings')}
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>

        );
    }
}