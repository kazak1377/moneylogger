/*
* @providesModule screens/expenses
*/
import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    View
} from "react-native";
import {
    Container,
    Header,
    Title,
    Row,
    Content,
    Button,
    Icon,
    ListItem,
    InputGroup,
    Input
} from "native-base";
import { Actions } from 'react-native-router-flux';
//import ExpensesList from "components/expenses/List";
import Expense from "models/expense"
import ARList from "components/ARList"
import Strings from "resources/strings";
import globals from "resources/globals";
import styles from "resources/appStyles"

export default class ExpensesScreen extends Component {
	constructor(props) {
        super(props);
        if (props.currentProfile) {
            this.state = {
                currentProfile: props.currentProfile
            }
        } else {
            this.state = {
                currentProfile: false
            }
        }
	}

    render() {
        return (
            <Container>
                <Header>
                    <Button transparent onPress={() => {
                        Actions.refresh({key: 'drawer', open: value => !value })
                    }}>
                        <Icon name='ios-menu' />
                    </Button>
                    <Title >
                        {Strings.i18n('expensesList')}
                    </Title>
                    <Button transparent onPress={
                        () => {
                            Actions.creator({transactionType: 'expense', from: 'expenses'})
                        }
                    }>
                        <Icon name='ios-add' />
                    </Button>
                </Header>
                <Content style={{padding: 5}}>
                    <ARList
                        dataArray={Expense.find()}
                        toCreator="expenses"
                        icon={Expense.icon}
                        refresh={() => {
                            return Expense.find();
                        }}
                    />
                </Content>
            </Container>

        );
    }
}
