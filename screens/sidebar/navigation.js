/**
* @providesModule screens/sidebar/navigation
*/

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    View
} from "react-native";
import Drawer from 'react-native-drawer';
import {Actions, DefaultRenderer} from 'react-native-router-flux';
import SideMenu from 'screens/sidebar/sidemenu';

export default class Navigation extends Component {
    render(){
        const state = this.props.navigationState;
        const children = state.children;
        return (
            <Drawer
                ref="navigation"
                open={state.open}
                onOpen={()=>Actions.refresh({key:state.key, open: true})}
                onClose={()=>Actions.refresh({key:state.key, open: false})}
                type="overlay"
                styles={{
                    shadowColor: '#000000',
                    shadowOpacity: 1,
                    shadowRadius: 20,
                    shadowOffsetX: 100
                }}
                content={<SideMenu />}
                tapToClose={true}
                acceptPan={true}
                captureGestures={true}
                openDrawerOffset={0.2}
                panCloseMask={0.2}
                panOpenMask={0.02}
                negotiatePan={true}
            >
                <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
            </Drawer>
        );
    }
}