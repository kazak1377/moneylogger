/*
* @providesModule screens/sidebar/sidemenu
*/

import React, {Component} from "react";
import {
    AppRegistry
} from "react-native";
import Drawer from 'react-native-drawer';
import {
	Actions,
	DefaultRenderer
} from 'react-native-router-flux';
import {
	Container,
	Content,
	Header,
	List,
	ListItem,
	Text
} from 'native-base';
import Strings from 'resources/strings';
import globals from 'resources/globals';


export default class SideMenu extends Component {
	constructor(props) {
        super(props);
        this.state = {
            menuItems: [
                'dashboard',
                'expenses',
                'incomes',
                'settings'
            ]
        }
    }

	openScreen(param) {
		Actions[param]();
		setTimeout(function() {
			Actions.refresh({key: 'drawer', open: value => !value });
		});
	}

	render() {
		return (
			<Container style={{backgroundColor: '#efefef'}}>
				<Content style={{marginTop: 50}}>
					<List
                        dataArray={this.state.menuItems}
                        renderRow={(item) => {
                            return (
                                <ListItem
                                    style={{
                                        height: 50,
                                        borderBottomWidth: 2,
                                        borderBottomColor: '#abc2e8'
                                    }}
                                    onPress={() => {
                                        this.openScreen(item)
                                    }}>
                                    <Text style={{fontSize: 18}}>
                                        {Strings.i18n(item)}
                                    </Text>
                                </ListItem>
                            )
                        }}>
                    </List>
				</Content>
			</Container>
		);
	}
}