/**
 * Init point of our app.
 */

import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ListView,
    StatusBar,
    View
} from "react-native";
import { Router, Scene } from 'react-native-router-flux';

import IncomesScreen from 'screens/incomes';
import ExpensesScreen from 'screens/expenses';
import CreatorScreen from 'screens/creator';
import Navigation from 'screens/sidebar/navigation';
import SettingsScreen from "screens/settings";
import ProfileSettingsScreen from "screens/profileSettings";
import ProfileEditScreen from "screens/profileEdit";
import DashboardScreen from "screens/dashboard";
import ProfileDataScreen from "screens/profileData";


class MoneyLogger extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router hideNavBar={true} hideTabBar={true} style={{flex: 1}}>
                <StatusBar
                    backgroundColor="blue"
                    barStyle="light-content"
                />
                <Scene key="drawer" component={Navigation} open={false}>
                    <Scene key="root" panHandlers={null}>
                        <Scene key="dashboard" component={DashboardScreen} title="Dashboard" />
                        <Scene key="profileSettings" component={ProfileSettingsScreen}
                               title="ProfileSettings" />
                        <Scene key="settings" component={SettingsScreen} title="Incomes" />
                        <Scene key="expenses" component={ExpensesScreen} title="Expenses" />
                        <Scene key="creator" component={CreatorScreen} title="Create" />
                        <Scene key="incomes" component={IncomesScreen} title="Incomes" />
                        <Scene key="profileEdit" component={ProfileEditScreen}
                               title="ProfileEdit" />
                        <Scene key="profileData" component={ProfileDataScreen}
                               title="ProfileData" />
                    </Scene>
                </Scene>
            </Router>
        );
    }
}

AppRegistry.registerComponent('MoneyLogger', () => MoneyLogger);
