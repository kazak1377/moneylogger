/**
 * Init point of our app.
 */

import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    ListView,
    View
} from "react-native";
import { Router, Scene } from 'react-native-router-flux';

import IncomesScreen from 'app/screens/incomes';
import ExpensesScreen from 'app/screens/expenses';
import CreatorScreen from 'app/screens/creator';
import Navigation from 'app/screens/sidebar/navigation';
import SettingsScreen from "app/screens/settings";
import ProfileSettingsScreen from "app/screens/profileSettings";
import ProfileEditScreen from "app/screens/profileEdit";
import DashboardScreen from "app/screens/dashboard";
import ProfileDataScreen from "app/screens/profileData";


class MoneyLogger extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router hideNavBar={true} hideTabBar={true}>
                <Scene key="drawer" component={Navigation} open={false}>
                    <Scene key="root" panHandlers={null}>
                        <Scene key="dashboard" component={DashboardScreen} title="Dashboard" />
                        <Scene key="profileSettings" component={ProfileSettingsScreen}
                               title="ProfileSettings" />
                        <Scene key="settings" component={SettingsScreen} title="Incomes" />
                        <Scene key="expenses" component={ExpensesScreen} title="Expenses" />
                        <Scene key="creator" component={CreatorScreen} title="Create" />
                        <Scene key="incomes" component={IncomesScreen} title="Incomes" />
                        <Scene key="profileEdit" component={ProfileEditScreen}
                               title="ProfileEdit" />
                        <Scene key="profileData" component={ProfileDataScreen}
                               title="ProfileData" />
                    </Scene>
                </Scene>
            </Router>
        );
    }
}

AppRegistry.registerComponent('moneyLogger02', () => MoneyLogger);