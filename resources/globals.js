/*
* @providesModule resources/globals
*/
import Settings from "models/settings";
import Profiles from "models/profiles";

export default globals = {
	settings: {
		lang : Settings.getValue('lang') ? Settings.getValue('lang') : "ru",
		curr : Settings.getValue('curr') ? Settings.getValue('curr') : "UAH",
		currentProfile : Profiles.find()[0] ? Profiles.find()[0] : new Profiles()
	}
}