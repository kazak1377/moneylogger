/*
* @providesModule resources/strings
*/
import globals from "resources/globals";
strings = {
    expense : {
        ru: "Расход",
        en: "Expense"
    },
    amount : {
        ru: "Количество",
        en: "Amount"
    },
    add : {
        ru : "Добавить",
        en: "Add"
    },
    expenses : {
        ru: "Расходы",
        en: "Expenses"
    },
    expensesList : {
        ru: "Рассходы",
        en: "Expenses"
    },
    income : {
        ru : "Доход",
        en : "Income"
    },
    incomes : {
        ru: "Доходы",
        en: "Incomes"
    },
    selectType: {
        ru: "Выберите тип",
        en: "Select type"
    },
    comment : {
        ru: "Комментарий",
        en: "Comment"
    },
    delete : {
        ru: "Удалить",
        en: "Delete"
    },
    settings : {
        ru: "Настройки",
        en: "Settings"
    },
    language: {
        ru: "Язык",
        en: "Language"
    },
    currency: {
        ru: "Валюта",
        en: "Currency"
    },
    profileSettings : {
        ru: "Настройки профиля",
        en: "Profile settings"
    },
    profilesSettings : {
        ru: "Настройки профилей",
        en: "Profiles settings"
    },
    newProfile: {
        ru: "Новый профиль",
        en: "New profile"
    },
    selectIcon: {
        ru: "Выбери иконку",
        en: "Select icon"
    },
    profiles: {
        ru: "Профили",
        en: "Profiles"
    },
    profile: {
        ru: "Профиль",
        en: "Profile"
    },
    dashboard: {
        ru: "Сводка",
        en: "Dashboard"
    },
    totalBalance : {
        ru: "Общий баланс",
        en: "Total balance"
    },
    cancel : {
        ru: "Отмена",
        en: "Cancel"
    },
    confirm: {
        ru: "Подтвердить",
        en: "Confirm"
    },
    edit: {
        ru: "Редактировать",
        en: "Edit"
    },
    type: {
        ru: "Тип",
        en: "Type"
    },
    podium : {
        ru: "Подиум",
        en: "Podium"
    },
    woman : {
        ru: "Женщина",
        en: "Woman"
    },
    walk : {
        ru: "Прогулка",
        en: "Walk"
    },
    water : {
        ru: "Вода",
        en: "Water"
    },
    warning : {
        ru: "Внимание",
        en: "Warning"
    },
    videocam : {
        ru: "Камера",
        en: "Videocam"
    },
    umbrella : {
        ru: "Зонтик",
        en: "Umbrella"
    },
    trophy : {
        ru: "Трофей",
        en: "Trophy"
    },
    timer : {
        ru: "Таймер",
        en: "Timer"
    },
    stats : {
        ru: "Статистика",
        en: "Stats"
    },
    snow : {
        ru: "Снег",
        en: "Snow"
    },
    shirt : {
        ru: "Футболка",
        en: "Shirt"
    },
    school : {
        ru: "Школа",
        en: "School"
    },
    rose : {
        ru: "Роза",
        en: "Rose"
    },
    ribbon : {
        ru: "Медаль",
        en: "Ribbon"
    },
    print : {
        ru: "Печать",
        en: "Print"
    },
    planet : {
        ru: "Планета",
        en: "Planet"
    },
    plane : {
        ru: "Самолет",
        en: "Plane"
    },
    pizza : {
        ru: "Пицца",
        en: "Pizza"
    },
    usd : {
        ru: "Доллар",
        en: "Usd"
    },
    tux : {
        ru: "Тукс",
        en: "Tux"
    },
    title: {
        ru: "Название",
        en: "Title"
    },
    color: {
        ru: "Цвет",
        en: "Color"
    },
    red : {
        ru: "Красный",
        en: "Red"
    },
    lightBlue : {
        ru: "Голубой",
        en: "Light blue"
    },
    green : {
        ru: "Зеленый",
        en: "Green"
    },
    orange : {
        ru: "Оранженый",
        en: "Orange"
    },
    purple : {
        ru: "Фиолетовый",
        en: "Purple"
    },
    description: {
        ru: "Описание",
        en: "Description"
    },
    save: {
        ru: "Сохранить",
        en: "Save"
    },
    all: {
        ru: "Все",
        en: "All"
    }
}

export default class Strings {
    static i18n(key) {
        return strings[key][globals.settings.lang];
    }
}