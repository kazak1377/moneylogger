/**
 * @providesModule resources/appStyles
 */
export default styles = {
    blue: {
        navBar: {
            backgroundColor: '#63A6BC'
        },
        text : {
            fontSize: 22
        },
        input: {
            textAlign: 'right'
        }
    }
}